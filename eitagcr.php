<?php
/**
 * Plugin Name: EITA GCR
 * Version: 1.0.0
 * Plugin URI: http://www.hughlashbrooke.com/
 * Description: This is your starter template for your next WordPress plugin.
 * Author: Hugh Lashbrooke
 * Author URI: http://www.hughlashbrooke.com/
 * Requires at least: 4.0
 * Tested up to: 4.0
 *
 * Text Domain: eitagcr
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Hugh Lashbrooke
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Load plugin class files.
require_once 'includes/class-eitagcr.php';
require_once 'includes/class-eitagcr-settings.php';

// Load plugin libraries.
require_once 'includes/lib/class-eitagcr-admin-api.php';
require_once 'includes/lib/class-eitagcr-post-type.php';
require_once 'includes/lib/class-eitagcr-taxonomy.php';

require_once 'vendor/autoload.php';

/**
 * Returns the main instance of EitaGCR to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object EitaGCR
 */
function eitagcr() {
	$instance = EitaGCR::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = EitaGCR_Settings::instance( $instance );
	}

	return $instance;
}

eitagcr();
