<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * Settings class file.
 *
 * @package WordPress Plugin Template/Settings
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Settings class.
 */
class EitaGCR_Settings {

	/**
	 * The single instance of EitaGCR_Settings.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * The main plugin object.
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin settings.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = '';

	/**
	 * Available settings for plugin.
	 *
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $settings = array();

	/**
	 * Constructor function.
	 *
	 * @param object $parent Parent object.
	 */
	public function __construct( $parent ) {
		$this->parent = $parent;

		$this->base = 'eita_gcr_';

		// Initialise settings.
		add_action( 'init', array( $this, 'init_settings' ), 11 );

		// Register plugin settings.
		add_action( 'admin_init', array( $this, 'register_settings' ) );

		// Add settings page to menu.
		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );

		// Add settings link to plugins page.
		add_filter(
			'plugin_action_links_' . plugin_basename( $this->parent->file ),
			array(
				$this,
				'add_settings_link',
			)
		);

		// Configure placement of plugin settings page. See readme for implementation.
		add_filter( $this->base . 'menu_settings', array( $this, 'configure_settings' ) );
	}

	/**
	 * Initialise settings
	 *
	 * @return void
	 */
	public function init_settings() {
		$this->process_requests();
		$this->settings = $this->settings_fields();
	}

	/**
	 * Process requests before building settings
	 *
	 * @return void
	 */
	public function process_requests() {

		// Change front page according to eita_gcr options
		if ( isset( $_POST[ 'eita_gcr_page_open' ] ) && $_POST[ 'eita_gcr_page_open' ] ) {
			if( $_POST[ 'eita_gcr_cycle_open' ] == 'open' ){
				$page_id = $_POST[ 'eita_gcr_page_open' ];
			} else {
				$page_id = $_POST[ 'eita_gcr_page_closed' ];
			}
			update_option( 'page_on_front', $page_id );
		}

		// Create new cycle
		if ( isset( $_POST[ 'create_cycle' ] ) && ( $_POST[ 'create_cycle' ] === "yes" )) {
			$args = array(
				'post_type'      => 'cycle',
				'posts_per_page' => -1,
			);
			$cycles = get_posts( $args );
			$title = __( 'Cycle', 'eitagcr' ) . " " . ( count( $cycles ) + 1 );

			$cycle_id = wp_insert_post(
				array(
					'post_title'  => $title,
					'post_type'   => 'cycle',
					'post_status' => 'publish',
				),
			);

			// Initialize open and close datetime
			update_post_meta( $cycle_id, 'cycle_open_time', '0000-00-00 00:00:00' );
			update_post_meta( $cycle_id, 'cycle_close_time', '0000-00-00 00:00:00' );

			// Sets new cycle as active (??)
			update_option( 'eita_gcr_cycle_active', $cycle_id);

			// Sets values for the Cycle tab
			update_option( 'eita_gcr_cycle_to_configure', $cycle_id);
			update_option( 'eita_gcr_cycle_title', $title );
			update_option( 'eita_gcr_cycle_open_time', '0000-00-00 00:00:00');
			update_option( 'eita_gcr_cycle_close_time', '0000-00-00 00:00:00');

			$cycle_delivery_places = get_post_meta( $cycle_id, 'cycle_delivery_places', true );
			update_option( 'eita_gcr_cycle_delivery_places', $cycle_delivery_places );
		}

		// Update Cycle title and meta
		if ( isset( $_POST[ 'eita_gcr_cycle_to_configure' ] ) && $_POST[ 'eita_gcr_cycle_to_configure' ] ) {

			$cycle_id = $_POST[ 'eita_gcr_cycle_to_configure' ];

			if ( isset( $_POST[ 'change_cycle' ] ) && $_POST[ 'change_cycle' ] ) {
				$cycle_open_time = get_post_meta( $cycle_id, 'cycle_open_time', true );
				$cycle_close_time = get_post_meta( $cycle_id, 'cycle_close_time', true );
				$_POST[ 'eita_gcr_cycle_open_time' ] = $cycle_open_time;
				$_POST[ 'eita_gcr_cycle_close_time' ] = $cycle_close_time;
				$_POST[ 'eita_gcr_cycle_title' ] = get_the_title( $cycle_id );
				$_POST[ 'eita_gcr_cycle_delivery_places' ] = get_post_meta( $cycle_id, 'cycle_delivery_places', true );
			} else {
				if( isset( $_POST['eita_gcr_cycle_title'] ) && $_POST['eita_gcr_cycle_title'] ) {
					$cycle = array(
			      'ID'         => $cycle_id,
			      'post_title' => $_POST[ 'eita_gcr_cycle_title' ],
					);
					wp_update_post( $cycle );
				}

				if( isset( $_POST[ 'eita_gcr_cycle_open_time' ] ) && $_POST[ 'eita_gcr_cycle_open_time' ] ) {
					update_post_meta( $cycle_id, 'cycle_open_time', $_POST['eita_gcr_cycle_open_time'] );
				}
				if( isset( $_POST[ 'eita_gcr_cycle_close_time' ] ) && $_POST[ 'eita_gcr_cycle_close_time' ] ) {
					update_post_meta( $cycle_id, 'cycle_close_time', $_POST[ 'eita_gcr_cycle_close_time' ] );
				}

				if( isset( $_POST[ 'eita_gcr_cycle_delivery_places' ] ) && $_POST[ 'eita_gcr_cycle_delivery_places' ] ) {
					update_post_meta(
						$cycle_id,
						'cycle_delivery_places',
						$_POST[ 'eita_gcr_cycle_delivery_places' ]
					);
				}
			}
		}

		// Generate Report
		if ( isset( $_POST[ 'generate_report' ] ) && $_POST[ 'generate_report' ] ) {
			if ( $_POST[ 'generate_report' ] === 'yes' ) {
				$this->generate_report();
			}
		}

		// Download report
		if ( isset( $_POST[ 'download_report' ] ) && $_POST[ 'download_report' ] ) {

			$html = $this->get_report ( $_POST[ 'report' ] );

			$cycle = get_post( $_POST['cycle'] );

			$date = date('m/d/Y h:i:s a', time());
			$header = "<tr><td colspan='4'>Relatório gerado em $date, referente ao ciclo {$cycle->post_title}</td></tr><tr><td style='height: 30px' colspan='4'>&nbsp;</td></tr>";

			$fid = fopen( ABSPATH . 'wp-content/uploads/' . 'report.html', 'w' );
			fwrite( $fid, $header . $html );
			fclose( $fid );

			$reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
			$spreadsheet = $reader->load(ABSPATH . 'wp-content/uploads/' . 'report.html');
			$writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);

			if ( $_POST[ 'report_format' ]  === 'PDF') {
				$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Mpdf');
				$filename = ABSPATH . 'wp-content/uploads/' . 'report.pdf';
				$writer->save( $filename );
				$ctype = "application/pdf";
			} elseif ( $_POST[ 'report_format' ]  === 'XLSX' ) {
				$filename = ABSPATH . 'wp-content/uploads/' . 'report.xlsx';
				$writer->save( $filename );
				$ctype = "application/vnd.ms-excel";
			}

			header("Pragma: public"); // required
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: $ctype");
			header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".filesize($filename));
			readfile("$filename");
			// exit();
		}

		// Split Clients Orders
		if ( isset( $_POST[ 'split_client_orders' ] ) && $_POST[ 'split_client_orders' ] ) {

			$cycle_id = get_option( 'eita_gcr_cycle_active' );

			# get Orders
			$args = array(
				'post_type' => 'shop_order',
				'meta_key' => '_eita_gcr_cycle_id',
				'meta_value' => $cycle_id,
				'posts_per_page' => -1,
				'post_status'    => 'wc-processing',
			);
			$orders = get_posts( $args );

			foreach ($orders as $original_order) {
				# get all suppliers in order;
				$original_order = wc_get_order( $original_order->ID );
				$user_id = $original_order->get_user()->ID;
				$suppliers_items = [];
				foreach( $original_order->get_items() as $item_id => $item ){
					$product_id = $item->get_product_id();
					$supplier = wp_get_post_terms( $product_id, 'supplier' )[0];
					$suppliers_items[$supplier->term_id][] = $item;
				}

				# foreach supplier, create an order;
				foreach ( $suppliers_items as $supplier => $items ) {
					$order = wc_create_order();
					$order->user_id = $user_id;
					update_post_meta($order->get_id(), '_customer_user', $user_id);
					foreach ($items as $item) {
						$order->add_product( $item->get_product(), $item->get_quantity());
					}
				}

				# o que fazer o pedido original?
				# como impedir criação de vários?

			}

		}

	}


	/**
	 * Add settings page to admin menu
	 *
	 * @return void
	 */
	public function add_menu_item() {

		$args = $this->menu_settings();

		// Do nothing if wrong location key is set.
		if ( is_array( $args ) && isset( $args['location'] ) && function_exists( 'add_' . $args['location'] . '_page' ) ) {
			switch ( $args['location'] ) {
				case 'options':
				case 'submenu':
					$page = add_submenu_page( $args['parent_slug'], $args['page_title'], $args['menu_title'], $args['capability'], $args['menu_slug'], $args['function'] );
					break;
				case 'menu':
					$page = add_menu_page( $args['page_title'], $args['menu_title'], $args['capability'], $args['menu_slug'], $args['function'], $args['icon_url'], $args['position'] );
					add_submenu_page( $args['menu_slug'], $args['page_title'], $args['menu_title'], $args['capability'], $args['menu_slug'], $args['function'], 0 );
					break;
				default:
					return;
			}
			add_action( 'admin_print_styles-' . $page, array( $this, 'settings_assets' ) );
		}
	}

	/**
	 * Prepare default settings page arguments
	 *
	 * @return mixed|void
	 */
	private function menu_settings() {
		return apply_filters(
			$this->base . 'menu_settings',
			array(
				'location'    => 'menu', // Possible settings: options, menu, submenu.
				'parent_slug' => 'options-general.php',
				'page_title'  => __( 'Consumo Responsável', 'eitagcr' ),
				'menu_title'  => __( 'Consumo Responsável', 'eitagcr' ),
				'capability'  => 'manage_woocommerce',
				'menu_slug'   => $this->parent->_token . '_settings',
				'function'    => array( $this, 'settings_page' ),
				'icon_url'    => '',
				'position'    => null,
			)
		);
	}

	/**
	 * Container for settings page arguments
	 *
	 * @param array $settings Settings array.
	 *
	 * @return array
	 */
	public function configure_settings( $settings = array() ) {
		return $settings;
	}

	/**
	 * Load settings JS & CSS
	 *
	 * @return void
	 */
	public function settings_assets() {

		// We're including the farbtastic script & styles here because they're needed for the colour picker
		// If you're not including a colour picker field then you can leave these calls out as well as the farbtastic dependency for the wpt-admin-js script below.
		wp_enqueue_style( 'farbtastic' );
		wp_enqueue_script( 'farbtastic' );

		// We're including the WP media scripts here because they're needed for the image upload field.
		// If you're not including an image upload then you can leave this function call out.
		wp_enqueue_media();

		wp_register_script( $this->parent->_token . '-settings-js', $this->parent->assets_url . 'js/settings' . $this->parent->script_suffix . '.js', array( 'farbtastic', 'jquery' ), '1.0.0', true );
		wp_enqueue_script( $this->parent->_token . '-settings-js' );
	}

	/**
	 * Add settings link to plugin list table
	 *
	 * @param  array $links Existing links.
	 * @return array        Modified links.
	 */
	public function add_settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=' . $this->parent->_token . '_settings">' . __( 'Settings', 'eitagcr' ) . '</a>';
		array_push( $links, $settings_link );
		return $links;
	}

	/**
	 * Build settings fields
	 *
	 * @return array Fields to be displayed on settings page
	 */
	private function settings_fields() {

		$args = array('post_type' => 'page', 'post_status' => 'publish', 'posts_per_page' => -1);
		$allpages = get_posts( $args );
		foreach ($allpages as $page) {
			$pages[$page->ID] = $page->post_title;
		}

		$args = array('post_type' => 'cycle', 'post_status' => 'publish', 'posts_per_page' => -1);
		$allcycles = get_posts( $args );

		$cycles = null;
		if ( count( $allcycles ) > 0 ) {
			foreach ($allcycles as $cycle) {
				$cycles[$cycle->ID] = $cycle->post_title;
			}
		} else {
			$cycles = array( 0 => __( 'No cycle', 'eitagcr' ), );
		}

		if (get_option( 'eita_gcr_enable_delivery_place' ) == True) {
			$args = array('post_type' => 'deliveryplace', 'post_status' => 'publish', 'posts_per_page' => -1);
			$all_delivery_places = get_posts( $args );
			if ( count( $all_delivery_places ) > 0 ) {
				$delivery_places = NULL;
				foreach ( $all_delivery_places as $dp ) {
					$delivery_places[$dp->ID] = $dp->post_title;
				}
			}
		}

		$settings['general'] = array(
			'title'       => __( 'General', 'eitagcr' ),
			'description' => __( 'General options for the EITA GCR plugin.', 'eitagcr' ),
			'fields'      => array(
				array(
					'id'          => 'force_cycle_openclose',
					'label'       => __( 'Force cycle opening', 'eitagcr' ),
					'description' => __( 'Set this option to define if the cycle is open or closed overwriting the cycle parameters.', 'eitagcr' ),
					'type'        => 'checkbox',
				),
				array(
					'id'          => 'cycle_open',
					'label'       => __( 'The buying cycle is open?', 'eitagcr' ),
					'description' => __( 'Define if the buying cycle is open. This option only takes effect if you set Yes at the option above.', 'eitagcr' ),
					'type'        => 'radio',
					'options'     => array(
						'open' => __( 'Open', 'eitagcr' ),
						'closed'   => __( 'Closed', 'eitagcr' ),
					),
					'default'     => 'open',
				),
				array(
					'id'          => 'cycle_active',
					'label'       => __( 'Current cycle', 'eitagcr' ),
					'description' => __( 'Select the current open cycle.', 'eitagcr') . '
						<input type="hidden" name="create_cycle" value="yes" form="create_cycle">
						<input type="submit" value="' . __ ( 'Create new cycle', 'eitagcr') . '" form="create_cycle">',
					'type'        => 'select',
					'options'     => $cycles,
				),
				array(
					'id'          => 'page_open',
					'label'       => __( 'Home page when cycle is open', 'eitagcr' ),
					'description' => __( 'Choose the page used for home when cycle is open.', 'eitagcr' ),
					'type'        => 'select',
					'options'     => $pages,
				),
				array(
					'id'          => 'page_closed',
					'label'       => __( 'Home page when cycle is closed', 'eitagcr' ),
					'description' => __( 'Choose the page used for home when cycle is closed.', 'eitagcr' ),
					'type'        => 'select',
					'options'     => $pages,
				),
				array(
					'id'          => 'enable_delivery_place',
					'label'       => __( 'Enable delivery place feature', 'eitagcr' ),
					'description' => __( 'Mark here to be able to set different delivery places for each order.', 'eitagcr' ),
					'type'        => 'checkbox',
				),
			),
		);

		$settings['cycles'] = array(
			'title'       => __( 'Cycles', 'eitagcr' ),
			'description' => __( 'Cycles configurations', 'eitagcr' ),
			'fields'      => array(
				array(
					'id'          => 'cycle_to_configure',
					'label'       => __( 'Cycle to configure', 'eitagcr' ),
					'description' => __( 'Select the cycle to edit and see details', 'eitagcr' ),
					'type'        => 'select',
					'options'     => $cycles,
				),
				array(
					'id'          => 'cycle_title',
					'label'       => __( 'Cycle name', 'eitagcr' ),
					'description' => null,
					'placeholder' => __( 'Cycle 3/2020', 'eitagcr' ),
					'type'        => 'text',
				),
				array(
					'id'          => 'cycle_open_time',
					'label'       => __( 'Opening date and time', 'eitagcr' ),
					'description' => null,
					'placeholder' => __( 'aaaa-mm-dd hh:mm:ss', 'eitagcr' ),
					'type'        => 'text',
				),
				array(
					'id'          => 'cycle_close_time',
					'label'       => __( 'Closing date and time', 'eitagcr' ),
					'description' => null,
					'placeholder' => __( 'aaaa-mm-dd hh:mm:ss', 'eitagcr' ),
					'type'        => 'text',
				),
			),
		);

		if (get_option( 'eita_gcr_enable_delivery_place' ) == True) {
			$settings['cycles']['fields'][] = array(
				'id'          => 'cycle_delivery_places',
				'label'       => __( 'Cycle delivery places', 'eitagcr' ),
				'description' => __( 'Uncheck delivery place to disable it for this cycle', 'eitagcr' ),
				'type'        => 'checkbox_multi',
				'options'     => $delivery_places
			);
		}


		$settings['report_1'] = array(
			'title'       => __( 'Report Orders/Customers', 'eitagcr' ),
			'description' => __( 'This report shows the details of each order per customer', 'eitagcr' ),
			'fields'      => array(
				array(
					'id'          => 'cycle_report',
					'label'       => __( 'Cycle', 'eitagcr' ),
					'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
					'type'        => 'select',
					'options'     => $cycles,
				),
				array(
					'id'          => 'orderby_report',
					'label'       => __( 'Order by', 'eitagcr' ),
					'description' => __( 'Chosse how to order your report.', 'eitagcr' ),
					'type'        => 'select',
					'options'     => array(
															'DateTime' => __( 'Date', 'eitagcr' ),
															'FirstName' => __( 'Name', 'eitagcr' ),
														),
				),
			),
		);

		$settings['report_2'] = array(
			'title'       => __( 'Report Orders/Category', 'eitagcr' ),
			'description' => __( 'This report shows the details of each order per category', 'eitagcr' ),
			'fields'      => array(
				array(
					'id'          => 'cycle_report',
					'label'       => __( 'Cycle', 'eitagcr' ),
					'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
					'type'        => 'select',
					'options'     => $cycles,
				),
			),
		);

		$settings['report_3'] = array(
			'title'       => __( 'Report Orders/Category (Summary)', 'eitagcr' ),
			'description' => __( 'This report shows the details of each order per category', 'eitagcr' ),
			'fields'      => array(
				array(
					'id'          => 'cycle_report',
					'label'       => __( 'Cycle', 'eitagcr' ),
					'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
					'type'        => 'select',
					'options'     => $cycles,
				),
			),
		);

		// Enable report only if Delivery Place is active
		if (get_option( 'eita_gcr_enable_delivery_place' ) == True) {
			$settings['report_4'] = array(
				'title'       => __( 'Report Orders/Delivery Place', 'eitagcr' ),
				'description' => __( 'This report shows the order details of each delivery place', 'eitagcr' ),
				'fields'      => array(
					array(
						'id'          => 'cycle_report',
						'label'       => __( 'Cycle', 'eitagcr' ),
						'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
						'type'        => 'select',
						'options'     => $cycles,
					),
				),
			);
			$settings['report_5'] = array(
				'title'       => __( 'Report Category (Summary)/ Delivery Place', 'eitagcr' ),
				'description' => __( 'This report shows the summary for each catgory by delivery place', 'eitagcr' ),
				'fields'      => array(
					array(
						'id'          => 'cycle_report',
						'label'       => __( 'Cycle', 'eitagcr' ),
						'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
						'type'        => 'select',
						'options'     => $cycles,
					),
				),
			);
			$settings['report_6'] = array(
				'title'       => __( 'Report Category (Summary)/ Payment Method', 'eitagcr' ),
				'description' => __( 'This report shows the summary for each catgory by payment method', 'eitagcr' ),
				'fields'      => array(
					array(
						'id'          => 'cycle_report',
						'label'       => __( 'Cycle', 'eitagcr' ),
						'description' => __( 'Select the cycle to see the report.', 'eitagcr' ),
						'type'        => 'select',
						'options'     => $cycles,
					),
				),
			);
		}
		$settings = apply_filters( $this->parent->_token . '_settings_fields', $settings );

		return $settings;
	}

	/**
	 * Register plugin settings
	 *
	 * @return void
	 */
	public function register_settings() {

		if ( is_array( $this->settings ) ) {

			// Check posted/selected tab.
			//phpcs:disable
			$current_section = '';
			if ( isset( $_POST['tab'] ) && $_POST['tab'] ) {
				$current_section = $_POST['tab'];
			} else {
				if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
					$current_section = $_GET['tab'];
				}
			}
			//phpcs:enable

			foreach ( $this->settings as $section => $data ) {

				if ( $current_section && $current_section !== $section ) {
					continue;
				}

				// Add section to page.
				add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), $this->parent->_token . '_settings' );

				foreach ( $data['fields'] as $field ) {

					// Validation callback for field.
					$validation = '';
					if ( isset( $field['callback'] ) ) {
						$validation = $field['callback'];
					}

					// Register field.
					$option_name = $this->base . $field['id'];
					register_setting( $this->parent->_token . '_settings', $option_name, $validation );

					// Add field to page.
					add_settings_field(
						$field['id'],
						$field['label'],
						array( $this->parent->admin, 'display_field' ),
						$this->parent->_token . '_settings',
						$section,
						array(
							'field'  => $field,
							'prefix' => $this->base,
						)
					);
				}

				if ( ! $current_section ) {
					break;
				}
			}
		}
	}

	/**
	 * Settings section.
	 *
	 * @param array $section Array of section ids.
	 * @return void
	 */
	public function settings_section( $section ) {
		$html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
		echo $html; //phpcs:ignore
	}

	/**
	 * Load settings page content.
	 *
	 * @return void
	 */
	public function settings_page() {

		// Build page HTML.
		$html      = '<div class="wrap" id="' . $this->parent->_token . '_settings">' . "\n";
			$html .= '<h2>' . __( 'Consumo Responsável', 'eitagcr' ) . '</h2>' . "\n";

			$html .= '<div>' . __( 'Cycle is ', 'eitagcr' );
			$cycle_status = get_option( 'eita_gcr_cycle_open' );
			if ( $cycle_status === 'open' ) {
				$html .= __( 'Open', 'eitagcr' );
			} else {
				$html .= __( 'Closed', 'eitagcr' );
			}
			$html .= '</div>' . "\n";


			$tab = '';
		//phpcs:disable
		if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
			$tab .= $_GET['tab'];
		}
		//phpcs:enable

		// Show page tabs.
		if ( is_array( $this->settings ) && 1 < count( $this->settings ) ) {

			$html .= '<h2 class="nav-tab-wrapper">' . "\n";

			$c = 0;
			foreach ( $this->settings as $section => $data ) {
				// Set tab class.
				$class = 'nav-tab';
				if ( ! isset( $_GET['tab'] ) ) { //phpcs:ignore
					if ( 0 === $c ) {
						$class .= ' nav-tab-active';
					}
				} else {
					if ( isset( $_GET['tab'] ) && $section == $_GET['tab'] ) { //phpcs:ignore
						$class .= ' nav-tab-active';
					}
				}

				// Set tab link.
				$tab_link = add_query_arg( array( 'tab' => $section ) );
				if ( isset( $_GET['settings-updated'] ) ) { //phpcs:ignore
					$tab_link = remove_query_arg( 'settings-updated', $tab_link );
				}

				// Output tab.
				$html .= '<a href="' . $tab_link . '" class="' . esc_attr( $class ) . '">' . esc_html( $data['title'] ) . '</a>' . "\n";

				++$c;
			}

			$html .= '</h2>' . "\n";
		}

				$html .= '<form method="post" action="options.php" enctype="multipart/form-data">' . "\n";
				// Get settings fields.
				ob_start();
				settings_fields( $this->parent->_token . '_settings' );
				do_settings_sections( $this->parent->_token . '_settings' );

				$html .= ob_get_clean();

					$html     .= '<p class="submit">' . "\n";
						$html .= '<input type="hidden" name="tab" value="' . esc_attr( $tab ) . '" />' . "\n";
						$html .= '<input name="Submit" type="submit" class="button-primary" value="' . esc_attr( __( 'Save Settings', 'eitagcr' ) ) . '" />' . "\n";
					$html     .= '</p>' . "\n";
				$html         .= '</form>' . "\n";

				if( explode("_", $tab)[0] == 'report') {
					$html .= $this->get_report( $tab );
				}

		$html             .= '</div>' . "\n";

		if( $tab == 'cycles') {

			$html .= '<form id="split_client_orders" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
				<input form="split_client_orders" type="hidden" name="split_client_orders" value="yes">
				<input form="split_client_orders" type="submit" value="' . __('Separar pedidos dos clientes', 'eitagcr') . '"
				<span class="description">'. __("Após o termino do ciclo, clique aqui para gerar os pedidos separados de cada demandante para cada ofertante.", "eitagcr").'</span>
			</form>';

			$html .= '<form id="create_supplier_order" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
				<input form="create_supplier_order" type="hidden" name="create_supplier_order" value="yes">
				<input form="create_supplier_order" type="submit" value="' . __('Criar/Atualizar pedidos aos ofertantes', 'eitagcr') . '"
				<span class="description">'. __("Após o termino do ciclo, clique aqui para gerar os pedidos aos ofertantes.", "eitagcr").'</span>
			</form>';

			$html .= '<form id="update_clients_orders" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles">
				<input form="update_clients_orders" type="hidden" name="update_clients_orders" value="yes">
				<input form="update_clients_orders" type="submit" value="' . __('Atualizar pedidos dos demandantes', 'eitagcr') . '"
				<span class="description">'. __("Após confirmação das taxas e frete nos pedidos aos ofertantes, clique aqui para atualizar os pedidos aos demandates.", "eitagcr").'</span>
			</form>';
		}

		if( explode('_', $tab)[0] == 'report' ) {

			$cycle_id = get_option( 'eita_gcr_cycle_report' );

			$html .= '
				<form id="download_report_xlsx" method="POST" action="options-general.php?page=eitagcr_settings">
					<input form="download_report_xlsx" type="hidden" name="download_report" value="yes">
					<input form="download_report_xlsx" type="hidden" name="report_format" value="XLSX">
					<input form="download_report_xlsx" type="hidden" name="cycle" value="'.$cycle_id.'">
					<input form="download_report_xlsx" type="hidden" name="report" value="'.$_GET['tab'].'">
					<input form="download_report_xlsx" type="submit" value="XLSX">
				</form>
				<form id="download_report_pdf" method="POST" action="options-general.php?page=eitagcr_settings">
					<input form="download_report_pdf" type="hidden" name="download_report" value="yes">
					<input form="download_report_pdf" type="hidden" name="report_format" value="PDF">
					<input form="download_report_pdf" type="hidden" name="cycle" value="'.$cycle_id.'">
					<input form="download_report_pdf" type="hidden" name="report" value="'.$_GET['tab'].'">
					<input form="download_report_pdf" type="submit" value="PDF">
				</form>
			';
		}

		if( $tab == 'general' || $tab == '' ) {
			$html .= '<form id="create_cycle" method="POST" action="options-general.php?page=eitagcr_settings&tab=cycles"></form>';

			$html .= '<form id="generate_report" method="POST" action="options-general.php?page=eitagcr_settings&tab=general">
				<input form="generate_report" type="hidden" name="generate_report" value="yes">
				<input form="generate_report" type="submit" value="' . __('Generate Report', 'eitagcr') . '"
			</form>';
		}

		echo $html; //phpcs:ignore
	}

	public function generate_report () {

		global $wpdb;
		$wpdb->get_results( "TRUNCATE TABLE {$wpdb->prefix}eitagcr_report" );

		$args = array(
			'post_type' => 'shop_order',
			'posts_per_page' => -1,
			'post_status' => [ 'wc-processing', 'wc-on-hold', 'wc-completed', 'wc-pending' ],
		);

		$orders = get_posts( $args );

		foreach ($orders as $order) {
			EitaGCR::eitagcr_add_to_report( $order->ID );
		}
	}

	public function get_report ( $report ) {

		global $wpdb;

		$cycle_id = get_option( 'eita_gcr_cycle_report' );
		if( !$cycle_id ){
			$cycle = get_posts( array( 'post_type' => 'cycle', 'posts_per_page' => 1))[0];
			$cycle_id = $cycle->ID;
		}
		$cycle = get_post($cycle_id);

		$total_cycle_price = $wpdb->get_results( "
			SELECT (sum(`order_item_price`) + sum(`order_item_tax`)) AS 'Total' FROM `{$wpdb->prefix}eitagcr_report` WHERE `cycle_id` = {$cycle_id}
		", OBJECT );

		$total_cycle_price = $total_cycle_price[0]->Total;

		if ( $report == 'report_1' ) {

			$orderby = get_option( 'eita_gcr_orderby_report' );

			$orders = $wpdb->get_results( "
				SELECT
				`order_id`,
				sum(`order_item_price`) AS TotalPrice,
				sum(`order_item_tax`) AS TotalTax,
				max(`order_first_name`) AS FirstName,
				max(`order_last_name`) AS LastName,
				max(`order_datetime`) AS DateTime,
				max(`order_payment_method_title`) AS PaymentMethod
				FROM `{$wpdb->prefix}eitagcr_report`
				WHERE `cycle_id` = {$cycle_id}
				GROUP BY `order_id`
				ORDER BY $orderby
			", OBJECT );

			$html = "<table class='gcr_report'>";

			$html .= "
				<tr class='gcr_report_header'>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Qty', 'eitagcr') . "</th>
					<th style='text-align: center; width: 80px; border: 1px solid black'>" . __('Product', 'eitagcr') . "</th>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Unit price', 'eitagcr') . "</th>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Line price', 'eitagcr') . "</th>
				</tr>
			";

			foreach ($orders as $order) {
				$html .= "
				<tr class='gcr_report_orderid'>
					<td style='text-align: center; background-color: #CCC; border: 1px solid black; height: 15px; vertical-align: center' colspan='4'>{$order->FirstName} {$order->LastName} ({$order->order_id})</td>
				</tr>";

				$order_items = $wpdb->get_results( "
					SELECT * FROM `{$wpdb->prefix}eitagcr_report`
					WHERE `order_id` = {$order->order_id}
				", OBJECT );

				foreach ($order_items as $order_item) {
					$html .= "
						<tr class='gcr_report_orderdesc'>
						<td style='border: 1px solid black; text-align: center'>{$order_item->order_item_quantity}</td>
						<td style='border: 1px solid black;'>{$order_item->order_item_product}</td>
						<td style='border: 1px solid black;'>R$ " . number_format($order_item->order_item_unit_price,2, ",", ".") . "</td>
						<td style='border: 1px solid black;'>R$ " . number_format($order_item->order_item_price,2, ",", ".") . "</td>
						</tr>
						";
				}
				$html .= "
				<tr class='gcr_report_ordertotal'>
					<td  style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right' colspan='4'>" . __('Total', 'eitagcr') . ": R$ " . number_format($order->TotalPrice, 2, ",", ".") . " (" . $order->PaymentMethod . ")</td>
				</tr>";

				if( $order->TotalTax > 0 ){
					$html .= "
					<tr class='gcr_report_ordertotal'>
						<td  style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right' colspan='4'>" . __('Total com margem', 'eitagcr') . ": R$ " . number_format($order->TotalPrice+$order->TotalTax, 2, ",", ".") . "</td>
					</tr>";
				}
			}
		}

		if ( $report == 'report_2' ) {
			$terms = $wpdb->get_results("
				SELECT DISTINCT `order_item_category_id` AS 'TermID',
				`order_item_category` AS 'TermName'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id}
			", OBJECT );

			$html = "<table class='gcr_report'>";

			$html .= "
				<tr class='gcr_report_header'>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Qty', 'eitagcr') . "</th>
					<th style='text-align: center; width: 50px; border: 1px solid black'>" . __('Product', 'eitagcr') . "</th>
					<th style='text-align: center; width: 30px; border: 1px solid black'>" . __('Order', 'eitagcr') . "</th>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Unit price', 'eitagcr') . "</th>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Line price', 'eitagcr') . "</th>
				</tr>
			";


			foreach ($terms as $term) {

				$html .= "
				<tr class='gcr_report_orderid'>
					<td style='text-align: center; background-color: #CCC; border: 1px solid black; height: 15px; vertical-align: center' colspan='5'>$term->TermName</td>
				</tr>";

				$order_items = $wpdb->get_results( "
				SELECT order_item_product AS 'Product',
				order_first_name AS 'FirstName',
				order_last_name AS 'LastName',
				order_id AS 'OrderID',
				order_item_quantity AS 'Quantity',
				order_item_tax AS 'Tax',
				order_item_unit_tax AS 'UnitTax',
				order_item_price AS 'Price',
				order_item_unit_price AS 'UnitPrice'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id} AND `order_item_category_id` = $term->TermID
				", OBJECT );

				$order_total_price = 0;
				$order_total_tax = 0;

				foreach ($order_items as $order_item) {
					$html .= "
						<tr class='gcr_report_orderdesc'>
						<td style='border: 1px solid black; text-align: center'>{$order_item->Quantity}</td>
						<td style='border: 1px solid black;'>{$order_item->Product}</td>
						<td style='border: 1px solid black;'>{$order_item->FirstName} {$order_item->LastName} ({$order_item->OrderID})</td>
						<td style='border: 1px solid black;'>R$ " . number_format( $order_item->UnitPrice, 2, ",", "." ) . "</td>
						<td style='border: 1px solid black;'>R$ " . number_format( $order_item->Price, 2, ",", "." ) . "</td>
						</tr>
						";
					$order_total_price += $order_item->Price;
					$order_total_tax += $order_item->Tax;
				}
				$html .= "
				<tr class='gcr_report_ordertotal'>
					<td style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right' colspan='5'>" . __('Total', 'eitagcr') . ": R$ " . number_format($order_total_price, 2, ",", ".") . "</td>
				</tr>";
			}
			if( $order_total_tax > 0 ){
				$html .= "
				<tr class='gcr_report_ordertotal'>
					<td  style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right' colspan='4'>" . __('Total com margem', 'eitagcr') . ": R$ " . number_format($order_total_price+$order_total_tax, 2, ",", ".") . "</td>
				</tr>";
			}

		}

		if ( $report == 'report_3' ) {
			$terms = $wpdb->get_results("
				SELECT DISTINCT `order_item_category_id` AS 'TermID',
				`order_item_category` AS 'TermName'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id}
			", OBJECT );

			$html = "<table class='gcr_report' style='border: 1px solid black'>";

			$html .= "
				<tr class='gcr_report_header'>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Qty', 'eitagcr') . "</th>
					<th style='text-align: center; width: 80px; border: 1px solid black'>" . __('Product', 'eitagcr') . "</th>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Unit price', 'eitagcr') . "</th>
					<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Line price', 'eitagcr') . "</th>
				</tr>
			";

			foreach ($terms as $term) {
				$order_items = $wpdb->get_results( "
				SELECT order_item_product AS 'Product',
				sum(order_item_quantity) AS 'Quantity',
				sum(order_item_tax) AS 'Tax',
				max(order_item_unit_tax) AS 'UnitTax',
				sum(order_item_price) AS 'Price',
				max(order_item_unit_price) AS 'UnitPrice'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id} AND `order_item_category_id` = $term->TermID
				GROUP BY `Product`
				", OBJECT );

				$order_total_price = 0;

				$html .= "
				<tr class='gcr_report_orderid'>
					<td style='text-align: center; background-color: #CCC; border: 1px solid black; height: 15px; vertical-align: center' colspan='4'>$term->TermName</td>
				</tr>";

				foreach ($order_items as $order_item) {
					$html .= "
						<tr class='gcr_report_orderdesc'>
						<td style='border: 1px solid black; text-align: center'>{$order_item->Quantity}</td>
						<td style='border: 1px solid black'>{$order_item->Product}</td>
						<td style='border: 1px solid black'>R$ " . number_format($order_item->UnitPrice,2, ",", ".") . "</td>
						<td style='border: 1px solid black'>R$ " . number_format($order_item->Price,2, ",", ".") . "</td>
						</tr>
						";
					$order_total_price += $order_item->Price;
				}
				$html .= "
				<tr class='gcr_report_ordertotal'>
					<td colspan='4' style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right'>" . __('Total', 'eitagcr') . ": R$ " . number_format($order_total_price,2, ",", ".") . "</td>
				</tr>";
			}
		}

		if ( $report == 'report_4' ) {

			$places = $wpdb->get_results( "
				SELECT DISTINCT `order_delivery_place_id` AS 'PlaceID',
				`order_delivery_place` AS 'PlaceName'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id}
			", OBJECT );

			$html = "";
			$html .= "<table class='gcr_report' style='page-break-after: always;'>";

			foreach ($places as $place) {
				$html .= "<tr><td colspan='5' style='font-size: 18px; text-align: center; padding: 10px 0; background-color: #DDD'>" . $place->PlaceName . "</td></tr>";

				$html .= "
					<tr class='gcr_report_header'>
						<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Qty', 'eitagcr') . "</th>
						<th style='text-align: center; width: 80px; border: 1px solid black'>" . __('Product', 'eitagcr') . "</th>
						<th style='text-align: center; width: 80px; border: 1px solid black'>" . __('Category', 'eitagcr') . "</th>
						<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Unit price', 'eitagcr') . "</th>
						<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Line price', 'eitagcr') . "</th>
					</tr>
				";

				$orders = $wpdb->get_results( "
					SELECT
					`order_id` AS 'OrderID',
					sum(`order_item_price`) AS TotalPrice,
					sum(`order_item_tax`) AS TotalTax,
					max(`order_first_name`) AS FirstName,
					max(`order_last_name`) AS LastName,
					max(`order_datetime`) AS DateTime,
					max(`order_payment_method_title`) AS PaymentMethod
					FROM `{$wpdb->prefix}eitagcr_report`
					WHERE `cycle_id` = {$cycle_id} AND `order_delivery_place_id` = $place->PlaceID
					GROUP BY `order_id`
				", OBJECT );

				foreach ($orders as $order) {
					$html .= "
					<tr class='gcr_report_orderid'>
						<td style='text-align: center; background-color: #CCC; border: 1px solid black; height: 15px; vertical-align: center' colspan='5'>{$order->FirstName} {$order->LastName} ({$order->OrderID})</td>
					</tr>";

					$order_items = $wpdb->get_results( "
						SELECT order_item_product AS 'Product',
						order_first_name AS 'FirstName',
						order_last_name AS 'LastName',
						order_id AS 'OrderID',
						order_item_quantity AS 'Quantity',
						order_item_tax AS 'Tax',
						order_item_unit_tax AS 'UnitTax',
						order_item_price AS 'Price',
						order_item_unit_price AS 'UnitPrice',
						order_item_category AS 'Category'
						FROM {$wpdb->prefix}eitagcr_report
						WHERE `order_id` = {$order->OrderID}
					", OBJECT );

					foreach ($order_items as $order_item) {

						$html .= "
							<tr class='gcr_report_orderdesc'>
							<td style='border: 1px solid black; text-align: center'>{$order_item->Quantity}</td>
							<td style='border: 1px solid black;'>{$order_item->Product}</td>
							<td style='border: 1px solid black;'>{$order_item->Category}</td>
							<td style='border: 1px solid black;'>R$ " . number_format( $order_item->UnitPrice, 2, ",", ".") . "</td>
							<td style='border: 1px solid black;'>R$ " . number_format( $order_item->Price, 2, ",", "." ) . "</td>
							</tr>
							";
					}
					$html .= "
					<tr class='gcr_report_ordertotal'>
						<td  style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right' colspan='5'>Total: R$ " . number_format( $order->TotalPrice, 2, ",", "." ) . " (" . $order->PaymentMethod . ")</td>
					</tr>";

					if( $order->TotalTax > 0 ){
						$html .= "
						<tr class='gcr_report_ordertotal'>
							<td  style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right' colspan='5'>" . __('Total com margem', 'eitagcr') . ": R$ " . number_format($order->TotalPrice+$order->TotalTax, 2, ",", ".") . "</td>
						</tr>";
					}
				}
			}
		}
		if ( $report == 'report_5' ) {

			$places = $wpdb->get_results( "
				SELECT DISTINCT `order_delivery_place_id` AS 'PlaceID',
				`order_delivery_place` AS 'PlaceName'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id}
			", OBJECT );

			$html = "";
			$html .= "<table class='gcr_report' style='page-break-after: always;'>";

			foreach ($places as $place) {
				$html .= "<tr><td colspan='4' style='font-size: 18px; text-align: center; padding: 10px 0; background-color: #DDD'>" . $place->PlaceName. "</td></tr>";
				$html .= "
					<tr class='gcr_report_header'>
						<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Qty', 'eitagcr') . "</th>
						<th style='text-align: center; width: 80px; border: 1px solid black'>" . __('Product', 'eitagcr') . "</th>
						<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Unit price', 'eitagcr') . "</th>
						<th style='text-align: center; width: 10px; border: 1px solid black'>" . __('Line price', 'eitagcr') . "</th>
					</tr>
				";
				$place_total_price = 0;

				$terms = $wpdb->get_results("
					SELECT DISTINCT `order_item_category_id` AS 'TermID',
					`order_item_category` AS 'TermName'
					FROM {$wpdb->prefix}eitagcr_report
					WHERE `cycle_id` = {$cycle_id} and `order_delivery_place_id` = {$place->PlaceID}
				", OBJECT );

				foreach ($terms as $term) {
					$html .= "
					<tr class='gcr_report_orderid'>
						<td style='text-align: center; background-color: #CCC; border: 1px solid black; height: 15px; vertical-align: center' colspan='4'>$term->TermName</td>
					</tr>";

					$order_items = $wpdb->get_results( "
					SELECT order_item_product AS 'Product',
					sum(order_item_quantity) AS 'Quantity',
					sum(order_item_tax) AS 'Tax',
					max(order_item_unit_tax) AS 'UnitTax',
					sum(order_item_price) AS 'Price',
					max(order_item_unit_price) AS 'UnitPrice'
					FROM {$wpdb->prefix}eitagcr_report
					WHERE order_item_category_id = {$term->TermID}
					AND order_delivery_place_id = {$place->PlaceID}
					AND cycle_id = {$cycle_id}
					GROUP BY `Product`
					", OBJECT );

					foreach ($order_items as $order_item) {
						$html .= "
							<tr class='gcr_report_orderdesc'>
							<td style='border: 1px solid black; text-align: center'>{$order_item->Quantity}</td>
							<td style='border: 1px solid black'>{$order_item->Product}</td>
							<td style='border: 1px solid black'>R$ " . number_format( $order_item->UnitPrice, 2, ",", "." ) . "</td>
							<td style='border: 1px solid black'>R$ " . number_format( $order_item->Price, 2, ",", "." ) . "</td>
							</tr>
							";
						$place_total_price += $order_item->Price;
					}
				}
				$html .= "
				<tr class='gcr_report_ordertotal'>
					<td colspan='5' style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right'>" . __('Total', 'eitagcr') . ": R$ " . number_format( $place_total_price, 2, ",", "." ) . "</td>
				</tr>";
			}
		}

		if ( $report == 'report_6' ) {

			$paymentmethods = $wpdb->get_results( "
				SELECT DISTINCT `order_payment_method_title` AS 'PaymentMethod'
				FROM {$wpdb->prefix}eitagcr_report
				WHERE `cycle_id` = {$cycle_id}
			", OBJECT );

			$html = "";
			$html .= "<table class='gcr_report' style='page-break-after: always;'>";

			foreach ($paymentmethods as $paymentmethod) {
				$html .= "<tr><td colspan='4' style='font-size: 18px; text-align: center; padding: 10px 0; background-color: #DDD'>" . $paymentmethod->PaymentMethod. "</td></tr>";
				$html .= "
					<tr class='gcr_report_header'>
						<th colspan='3' style='text-align: center; width: 80%; border: 1px solid black'>" . __('Order', 'eitagcr') . "</th>
						<th style='text-align: center; width: 20%; border: 1px solid black'>" . __('Order Price', 'eitagcr') . "</th>
					</tr>
				";
				$paymentmethod_total_price = 0;

				$terms = $wpdb->get_results("
					SELECT DISTINCT `order_item_category_id` AS 'TermID',
					`order_item_category` AS 'TermName'
					FROM {$wpdb->prefix}eitagcr_report
					WHERE `cycle_id` = {$cycle_id} and `order_payment_method_title` = '{$paymentmethod->PaymentMethod}'
				", OBJECT );

				foreach ($terms as $term) {
					$html .= "
					<tr class='gcr_report_orderid'>
						<td style='text-align: center; background-color: #CCC; border: 1px solid black; height: 15px; vertical-align: center' colspan='4'>$term->TermName</td>
					</tr>";

					$orders = $wpdb->get_results( "
					SELECT order_id AS 'OrderId',
					max(order_first_name) AS 'FirstName',
					max(order_last_name) AS 'LastName',
					sum(order_item_price) AS 'OrderPrice'
					FROM {$wpdb->prefix}eitagcr_report
					WHERE order_item_category_id = {$term->TermID}
					AND order_payment_method_title = '{$paymentmethod->PaymentMethod}'
					AND cycle_id = {$cycle_id}
					GROUP BY `OrderId`
					", OBJECT );

					$paymentmethod_cat_total_price = 0;
					foreach ($orders as $order) {
						$html .= "
							<tr class='gcr_report_orderdesc'>
							<td colspan='3' style='border: 1px solid black; text-align: center'>{$order->FirstName} {$order->LastName} ({$order->OrderId})</td>
							<td style='border: 1px solid black; text-align: right'>R$ " . number_format( $order->OrderPrice, 2, ",", "." ) . "</td>
							</tr>
							";
						$paymentmethod_total_price += $order->OrderPrice;
						$paymentmethod_cat_total_price += $order->OrderPrice;
					}
					$html .= "
						<tr class='gcr_report_orderdesc'>
						<td colspan='4' style='border: 1px solid black; text-align: right'>" . __('Total', 'eitagcr') . ": R$ " . number_format( $paymentmethod_cat_total_price, 2, ",", "." ) . "</td>
						</tr>
						";
				}
				$html .= "
				<tr class='gcr_report_ordertotal'>
					<td colspan='4' style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black; text-align: right'>" . __('Payment Method Total', 'eitagcr') . ": R$ " . number_format( $paymentmethod_total_price, 2, ",", "." ) . "</td>
				</tr>";
			}
		}


		// Total Cycle
		$html .= "
		<tr class='gcr_report_total'>
			<td  style='height: 30px; vertical-align: middle; font-weight: bold; border: 1px solid black;' colspan='4'>" . __('Total cycle', 'eitagcr') . ": R$ " . number_format($total_cycle_price,2, ",", ".") . "</td>
		</tr>";
		$html .= "</table>";

		return $html;
	}

	/**
	 * Main EitaGCR_Settings Instance
	 *
	 * Ensures only one instance of EitaGCR_Settings is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see EitaGCR()
	 * @param object $parent Object instance.
	 * @return object EitaGCR_Settings instance
	 */
	public static function instance( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of EitaGCR_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of EitaGCR_API is forbidden.' ) ), esc_attr( $this->parent->_version ) );
	} // End __wakeup()

}
