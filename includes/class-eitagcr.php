<?php
/**
 * Main plugin class file.
 *
 * @package WordPress Plugin Template/Includes
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Main plugin class.
 */
class EitaGCR {

	/**
	 * The single instance of EitaGCR.
	 *
	 * @var     object
	 * @access  private
	 * @since   1.0.0
	 */
	private static $_instance = null; //phpcs:ignore

	/**
	 * Local instance of EitaGCR_Admin_API
	 *
	 * @var EitaGCR_Admin_API|null
	 */
	public $admin = null;

	/**
	 * Settings class object
	 *
	 * @var     object
	 * @access  public
	 * @since   1.0.0
	 */
	public $settings = null;

	/**
	 * The version number.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $_version; //phpcs:ignore

	/**
	 * The token.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $_token; //phpcs:ignore

	/**
	 * The main plugin file.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $file;

	/**
	 * The main plugin directory.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $dir;

	/**
	 * The plugin assets directory.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $assets_dir;

	/**
	 * The plugin assets URL.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $assets_url;

	/**
	 * Suffix for JavaScripts.
	 *
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $script_suffix;

	/**
	 * Constructor funtion.
	 *
	 * @param string $file File constructor.
	 * @param string $version Plugin version.
	 */
	public function __construct( $file = '', $version = '1.0.0' ) {
		$this->_version = $version;
		$this->_token   = 'eitagcr';

		// Load plugin environment variables.
		$this->file       = $file;
		$this->dir        = dirname( $this->file );
		$this->assets_dir = trailingslashit( $this->dir ) . 'assets';
		$this->assets_url = esc_url( trailingslashit( plugins_url( '/assets/', $this->file ) ) );
		$this->vendor_url = esc_url( trailingslashit( plugins_url( '/vendor/', $this->file ) ) );

		$this->script_suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '';

		register_activation_hook( $this->file, array( $this, 'install' ) );

		// Load frontend JS & CSS.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ), 10 );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 10 );

		// Load admin JS & CSS.
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ), 10, 1 );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_styles' ), 10, 1 );

		// Load API for generic admin functions.
		if ( is_admin() ) {
			$this->admin = new EitaGCR_Admin_API();
		}

		// Handle localisation.
		$this->load_plugin_textdomain();
		add_action( 'init', array( $this, 'load_localisation' ), 0 );

		// Add cycle post type
		$this->register_post_type( 'cycle', __( 'Cycles', 'eitagcr' ), __( 'Cycle', 'eitagcr' ) );
		// Add cycle data to order
		add_action( 'woocommerce_new_order', array( $this, 'eitagcr_add_cycle_to_order' ));


		// Add supplier taxonomy
		$this->register_taxonomy( 'supplier', __( 'Suppliers', 'eitagcr' ), __( 'Supplier', 'eitagcr' ), array( 'product', 'shop_order' ),
			array(
				'hierarchical' => true,
				'show_admin_column' => true,
			)
		);

		if (get_option( 'eita_gcr_enable_delivery_place' ) == True) {
			// Add delivery place post type
			$this->register_post_type( 'deliveryplace', __( 'Delivery places', 'eitagcr' ), __( 'Delivery place', 'eitagcr' ) );
			// Add delivery place field hook
			add_filter( 'woocommerce_checkout_fields', array( $this, 'eitagrc_add_delivery_place_to_checkout') );

			add_action( 'save_post', array( $this, 'eitagrc_init_cycle'), 10, 3 );
		}


		// Define if cycle is open or closed
		$current_status = get_option( 'eita_gcr_cycle_open');
		if ( ! $current_status ) {
			update_option( 'eita_gcr_cycle_open', 'closed' );
		}
		if ( get_option( 'eita_gcr_force_cycle_openclose' ) !== 'on' ) {

			$cycle_id = get_option( 'eita_gcr_cycle_active' );
			$cycle_open_time = get_post_meta( $cycle_id, 'cycle_open_time', true );
			$cycle_close_time = get_post_meta( $cycle_id, 'cycle_close_time', true );
			if ( $cycle_open_time && $cycle_close_time ) {
				$open = new DateTime( $cycle_open_time , new DateTimeZone('America/Sao_Paulo') );
				$close = new DateTime( $cycle_close_time, new DateTimeZone('America/Sao_Paulo') );
				$now = new DateTime('NOW', new DateTimeZone('America/Sao_Paulo'));
				if ( $now > $open && $now < $close && $current_status === 'closed') {
					update_option( 'eita_gcr_cycle_open', 'open' );
				} elseif (( $now < $open || $now > $close ) && $current_status === 'open') {
					update_option( 'eita_gcr_cycle_open', 'closed' );
				}
			}
		}

		// Disable purchasability if cycle is closed
		if ( get_option( 'eita_gcr_cycle_open' ) === 'closed' ) {
				add_filter( 'woocommerce_is_purchasable', '__return_false');
		}

		// Define open/close page
		if ($current_status === 'closed') {
			$page = get_option( 'eita_gcr_page_closed' );
			update_option( 'page_on_front', $page );
		} else {
			$page = get_option( 'eita_gcr_page_open' );
			update_option( 'page_on_front', $page );
		}

		// After an order is payed, feed eitagcr table
		add_action( 'woocommerce_order_status_processing', array( $this, 'eitagcr_add_to_report' ));
		// After an order is cancelled, remove it from eitagcr table
		add_action( 'woocommerce_order_status_cancelled', array( $this, 'eitagcr_remove_from_report' ));


	} // End __construct ()

	/**
	 * Register post type function.
	 *
	 * @param string $post_type Post Type.
	 * @param string $plural Plural Label.
	 * @param string $single Single Label.
	 * @param string $description Description.
	 * @param array  $options Options array.
	 *
	 * @return bool|string|EitaGCR_Post_Type
	 */
	public function register_post_type( $post_type = '', $plural = '', $single = '', $description = '', $options = array() ) {

		if ( ! $post_type || ! $plural || ! $single ) {
			return false;
		}

		$post_type = new EitaGCR_Post_Type( $post_type, $plural, $single, $description, $options );

		return $post_type;
	}

	/**
	 * Wrapper function to register a new taxonomy.
	 *
	 * @param string $taxonomy Taxonomy.
	 * @param string $plural Plural Label.
	 * @param string $single Single Label.
	 * @param array  $post_types Post types to register this taxonomy for.
	 * @param array  $taxonomy_args Taxonomy arguments.
	 *
	 * @return bool|string|EitaGCR_Taxonomy
	 */
	public function register_taxonomy( $taxonomy = '', $plural = '', $single = '', $post_types = array(), $taxonomy_args = array() ) {

		if ( ! $taxonomy || ! $plural || ! $single ) {
			return false;
		}

		$taxonomy = new EitaGCR_Taxonomy( $taxonomy, $plural, $single, $post_types, $taxonomy_args );

		return $taxonomy;
	}

	public function eitagrc_add_delivery_place_to_checkout( $fields ) {

		$args = array(
			'post_type' => 'deliveryplace',
			'posts_per_page' => -1
		);

		$deliveryplaces = array();
		$places = get_posts ($args );

		$cycle = get_option( 'eita_gcr_cycle_active' );
		$places_enabled = get_post_meta( $cycle, 'cycle_delivery_places', true );

		// backwards compatibility
		if( !$places_enabled ){
			$places_enabled = [];
			foreach ($places as $place) {
				$places_enabled[] = $place->ID;
	    }
		}

		foreach ($places as $place) {
			if( in_array( $place->ID, $places_enabled )){
				$deliveryplaces[$place->ID] = $place->post_title;
			}
    }

		$fields['order']['shipping_deliveryplace'] = array(
			'type'        => 'radio',
			'label'       => __( 'Select the delivery place', 'eitagcr' ),
			'required'    => true,
			'options'     => $deliveryplaces
		);
		return $fields;
	}

	public function eitagrc_init_cycle ( $cycle_ID, $post, $update ) {
		// Acts only for newly created delivery places
		if( !$update ) {
			$args = array(
				'post_type' => 'deliveryplace',
				'posts_per_page' => -1
			);

			$deliveryplaces = array();
			$places = get_posts( $args );
			foreach ($places as $place) {
				$deliveryplaces[] = $place->ID;
	    }
			update_post_meta( $cycle_ID, 'cycle_delivery_places', $deliveryplaces);
		}
	}

	public function eitagcr_add_cycle_to_order( $order_id ) {
		$cycle = get_option( 'eita_gcr_cycle_active' );
		update_post_meta( $order_id, '_eita_gcr_cycle_id', $cycle );
	}

	public static function eitagcr_add_to_report( $order_id ) {

		global $wpdb;

		$order = get_post( $order_id );

		$order_items = $wpdb->get_results( "
			SELECT
			woi.order_item_id as 'OrderItemID',
			woi.order_item_name as 'Product',
			product_id.meta_value as 'ProductID',
			quantity.meta_value as 'Quantity',
			subtotal.meta_value/quantity.meta_value as 'UnitPrice',
			subtotal_tax.meta_value/quantity.meta_value as 'UnitTax',
			subtotal.meta_value as 'Price',
			subtotal_tax.meta_value as 'Tax'
			from {$wpdb->prefix}woocommerce_order_items `woi`
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta `product_id`
			ON woi.order_item_id = product_id.order_item_id AND product_id.meta_key = '_product_id'
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta `quantity`
			ON woi.order_item_id = quantity.order_item_id AND quantity.meta_key = '_qty'
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta `subtotal`
			ON woi.order_item_id = subtotal.order_item_id AND subtotal.meta_key = '_line_subtotal'
			INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta `subtotal_tax`
			ON woi.order_item_id = subtotal_tax.order_item_id AND subtotal_tax.meta_key = '_line_subtotal_tax'
			WHERE woi.order_id = {$order->ID};
		", OBJECT );

		$cycle_id = get_post_meta( $order_id, '_eita_gcr_cycle_id', true);

		if ( ! $cycle_id ) {
			return;
		}

		$order_datetime = $order->post_date;
		$order_first_name = get_post_meta( $order_id, '_billing_first_name', true);
		$order_last_name = get_post_meta( $order_id, '_billing_last_name', true);
		$order_delivery_place_id = get_post_meta( $order_id, '_shipping_deliveryplace', true);
		$order_payment_method_title = get_post_meta( $order_id, '_payment_method_title', true );
		if ( $order_delivery_place_id ) {
			$order_delivery_place = get_the_title( $order_delivery_place_id );
		} else {
			$order_delivery_place = "";
		}

		foreach ($order_items as $order_item) {

			$product_category = wp_get_post_terms( $order_item->ProductID, 'product_cat' );

			if ( count( $product_category ) == 0 ) {
				$product_category = ""; $product_category_id = "";
			} else {
				$product_category_id = $product_category[0]->term_id;
				$product_category = $product_category[0]->name;
			}

			$query = "
				INSERT INTO {$wpdb->prefix}eitagcr_report (
					`order_item_id`, `order_id`,	`cycle_id`, `order_datetime`,	`order_first_name`,	`order_last_name`, `order_delivery_place_id`, `order_delivery_place`, `order_payment_method_title`, `order_item_product_id`, `order_item_product`, `order_item_category_id`, `order_item_category`, `order_item_quantity`, `order_item_unit_price`, `order_item_unit_tax`, `order_item_price`, `order_item_tax`
				) VALUES (
					'$order_item->OrderItemID',
					'$order_id',
					'$cycle_id',
					'$order_datetime',
					'" . esc_sql($order_first_name) . "',
					'" . esc_sql($order_last_name) . "',
					'$order_delivery_place_id',
					'" . esc_sql($order_delivery_place) . "',
					'" . esc_sql($order_payment_method_title) . "',
					'$order_item->ProductID',
					'" . esc_sql($order_item->Product) . "',
					'$product_category_id',
					'" . esc_sql($product_category) . "',
					'$order_item->Quantity',
					'$order_item->UnitPrice',
					'$order_item->UnitTax',
					'$order_item->Price',
					'$order_item->Tax'
				);
			";
			$result = $wpdb->get_results( $query );
		}
	}

	public function eitagcr_remove_from_report( $order_id ) {

		global $wpdb;

		$result = $wpdb->get_results( "
			DELETE
			from {$wpdb->prefix}eitagcr_report
			WHERE `order_id` = {$order_id}" );

		if ( ! $result ){
			echo "Error deleting order from eitagcr: " . $order_id;
		}
	}

	/**
	 * Load frontend CSS.
	 *
	 * @access  public
	 * @return void
	 * @since   1.0.0
	 */
	public function enqueue_styles() {
		wp_register_style( $this->_token . '-frontend', esc_url( $this->assets_url ) . 'css/frontend.css', array(), $this->_version );
		wp_enqueue_style( $this->_token . '-frontend' );
	} // End enqueue_styles ()

	/**
	 * Load frontend Javascript.
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function enqueue_scripts() {
		wp_register_script( $this->_token . '-frontend', esc_url( $this->assets_url ) . 'js/frontend' . $this->script_suffix . '.js', array( 'jquery' ), $this->_version, true );
		wp_enqueue_script( $this->_token . '-frontend' );
	} // End enqueue_scripts ()

	/**
	 * Admin enqueue style.
	 *
	 * @param string $hook Hook parameter.
	 *
	 * @return void
	 */
	public function admin_enqueue_styles( $hook = '' ) {
		wp_register_style( $this->_token . '-admin', esc_url( $this->assets_url ) . 'css/admin.css', array(), $this->_version );
		wp_enqueue_style( $this->_token . '-admin' );

		wp_register_style( 'datetime-admin', esc_url( $this->vendor_url ) . 'datetimepicker/build/jquery.datetimepicker.min.css', array(), $this->_version );
		wp_enqueue_style( 'datetime-admin' );

	} // End admin_enqueue_styles ()

	/**
	 * Load admin Javascript.
	 *
	 * @access  public
	 *
	 * @param string $hook Hook parameter.
	 *
	 * @return  void
	 * @since   1.0.0
	 */
	public function admin_enqueue_scripts( $hook = '' ) {
		wp_register_script( $this->_token . '-admin', esc_url( $this->assets_url ) . 'js/admin' . $this->script_suffix . '.js', array( 'jquery' ), $this->_version, true );
		wp_enqueue_script( $this->_token . '-admin' );

		wp_register_script( 'datetimepicker-admin', esc_url( $this->vendor_url ) . 'datetimepicker/build/jquery.datetimepicker.full.min.js', $this->_version, true );
		wp_enqueue_script( 'datetimepicker-admin' );

	} // End admin_enqueue_scripts ()

	/**
	 * Load plugin localisation
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function load_localisation() {
		load_plugin_textdomain( 'eitagcr', false, dirname( plugin_basename( $this->file ) ) . '/lang/' );
	} // End load_localisation ()

	/**
	 * Load plugin textdomain
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function load_plugin_textdomain() {
		$domain = 'eitagcr';

		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, WP_LANG_DIR . '/' . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, false, dirname( plugin_basename( $this->file ) ) . '/lang/' );
	} // End load_plugin_textdomain ()

	/**
	 * Main EitaGCR Instance
	 *
	 * Ensures only one instance of EitaGCR is loaded or can be loaded.
	 *
	 * @param string $file File instance.
	 * @param string $version Version parameter.
	 *
	 * @return Object EitaGCR instance
	 * @see EitaGCR()
	 * @since 1.0.0
	 * @static
	 */
	public static function instance( $file = '', $version = '1.0.0' ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $file, $version );
		}

		return self::$_instance;
	} // End instance ()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Cloning of EitaGCR is forbidden' ) ), esc_attr( $this->_version ) );

	} // End __clone ()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, esc_html( __( 'Unserializing instances of EitaGCR is forbidden' ) ), esc_attr( $this->_version ) );
	} // End __wakeup ()

	/**
	 * Installation. Runs on activation.
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	public function install() {
		$this->_log_version_number();
		$this->_create_db();
	} // End install ()

	/**
	 * Log the plugin version number.
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	private function _log_version_number() { //phpcs:ignore
		update_option( $this->_token . '_version', $this->_version );
	} // End _log_version_number ()


	/**
	 * Create the necessary database tables
	 *
	 * @access  public
	 * @return  void
	 * @since   1.0.0
	 */
	private function _create_db() { //phpcs:ignore
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$table_name = $wpdb->prefix . 'eitagcr_report';

		$sql = "CREATE TABLE `$table_name` (
		  `order_item_id` int(11) NOT NULL,
		  `order_id` int(11) NOT NULL,
			`cycle_id` int(11) NOT NULL,
		  `order_datetime` datetime NOT NULL,
		  `order_first_name` text,
		  `order_last_name` text,
		  `order_delivery_place_id` int(11) DEFAULT NULL,
		  `order_delivery_place` text,
			`order_payment_method_title` text,
			`order_item_product_id` int(11) NOT NULL,
		  `order_item_product` text NOT NULL,
			`order_item_category_id` int(11) NOT NULL,
			`order_item_category` text NOT NULL,
		  `order_item_quantity` float NOT NULL,
		  `order_item_unit_price` float NOT NULL,
		  `order_item_unit_tax` float DEFAULT NULL,
		  `order_item_price` float NOT NULL,
		  `order_item_tax` float DEFAULT NULL
		) $charset_collate;

		ALTER TABLE `wp_eitagcr_report`
		  ADD PRIMARY KEY (`order_item_id`),
		  ADD KEY `order_id` (`order_id`),
			ADD KEY `cycle_id` (`cycle_id`),
			ADD KEY `product_id` (`product_id`),
			ADD KEY `category_id` (`category_id`);
		COMMIT;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	} // End _create_db ()

}
