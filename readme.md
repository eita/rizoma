# EITA GCR
Contributors: alantygel
Tags: wordpress, woocommerce, responsible consumer group
Requires at least: 5.6
Tested up to: 5.6
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Woocommerce adaptation for Responsible Consumer Groups.

## Description

This plugin offers some enhancements to the Woocommerce e-commerce platform to make it fit for using in Responsible Consumer Groups (GCR). It assumes GCRs are composed by several consumers and several suppliers, and the there is a buying cycle.

Some of the features are:

* Register a cycle, with a opening and closing time. During this period, consumers can order their products;
* Register several delivery places;
* Several reports are ofered per cycle: (i) Detailed orders; (ii) Detailed orders per supplier; (iii) Summary (total number of products) per supplier; (iv) Detailed orders per delivery place; (v) Detailed orders per delivery place and supplier;
* Reports are shown in HTML and can be exported to XLSX and PDF;

## Installation

Installing "EITA GCR" can be done either by searching for "EITA GCR" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
1. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
1. Activate the plugin through the 'Plugins' menu in WordPress

## Development

1. Git clone the repository
2. Create a vendor directory
3. Add PhpOffice to vendor: https://github.com/PHPOffice/PhpSpreadsheet/
4. Add DateTimePicker to vendor: https://github.com/xdan/datetimepicker

## Screenshots

1. General settings page
![image](/uploads/927f714d217f0b98ffed1944f363ad56/image.png)

2. Editing a cycle
![image](/uploads/b2a3f13fbb9f450513b19a2879c74c7f/image.png)

3. Report 1: Orders
![image](/uploads/bba9f7b6c9567cc4c199a81f59fc2627/image.png)

## Frequently Asked Questions


## Changelog


## Upgrade Notice
